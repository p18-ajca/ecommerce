# Projet E-commerce :

### Composition du groupe: Jim, Amine, Aurélien et Camille

## Présentation du projet :

Nous sommes partis sur un site de librairie en ligne qui possède des livres en stock que l'on peut
ajouter au panier, la possibilité de se connecter, de trier des livres par catégories...


## La maquette :

![Schéma Use Case](Maquette/accueil.png)
![Schéma Use Case](Maquette/categorie-navbarr.png)
![Schéma Use Case](Maquette/connexion.png)
![Schéma Use Case](Maquette/form incription.png)
![Schéma Use Case](Maquette/panier.png)


## Use Case :


![Schéma Use Case](UML/UserCase.png)



## Structure de la BDD :
![Schéma BDD](UML/e_commerceClasses.png)

### A noter :

- l'utilstateur peut être relié à une ou plusieurs cotégories. Il en va de même pour les
    produits.
- La table command_item permet de faire le lien entre la table des paniers et celle des produits.

## Liste des fonctionnalités :

Idéalement, les fonctionnalités disponibles à la fin du projet seraient :

- Pour le client :

    - Pouvoir créer un compte.
    - La possibilité pour un utilisateur de se connecter et de consulter le panier en cours.
    - Pouvoir consulter les produits disponibles triés par catégories.
    - Pouvoir Ajouter lesdits produits au panier.
    - Passer commande(Vue l'état de nos compétences actuelles : Changer le status de ma commande).
    - Pouvoir se déconnecter.
  

- Pour l'administrateur :
    
    - Consulter la liste des différentes données des différentes tables et les supprimer.
    - Ajouter un produit.
    - Publier une annonce.

## Les principales pages du site :
    
Pour le client :

- Une page d'accueil qui affiche des annonces.
- une page login pour se connecter.
- Une page créer un compte.
- Une page produit ou se trouvent les produits triés par catégories. 
- Une page panier pour consulter le panier en cours.

Pour l'admin :

- Une page liste des produits.
- Une page liste des annonces.
- Une page liste des utilisateurs.
- une page créer un produit.
- Une page créer une annonce...

## Remarques :

- Le responsive n'est pas très optimisé(euphémisme, la version desktop a un côté Picasso). 
- L'ajout d'un produit dans le panier bug un peu parfois.
- Le nom de la table Command n'est pas très approprié. À la base, elle s'appelait Order
mais c'était certainement un mot clé et on a dû le changer pour que JPA arrive à la créer.
- Beaucoup de problèmes de nommage et d'organisation, en particulier dans Angular.
- On n'a pas eu le temps de bien gérer les sécurités des controllers dans le Back.
- Manque de validation et de réponses à l'utilisateur pour certains formulaires(Ajout de produit, Ajout d'annonce...).
- L'affichage des prix est mal géré et le total du panier est absent.
- Les tableaux du côté admin ne sont pas responsive et rendent mal en mobile(On s'en est rendu compte vers la fin).
        
        

